<?php
/**
 * Created by PhpStorm.
 * User: lichrok
 * Date: 11.07.2018
 * Time: 16:33
 */

namespace app\models;

use Yii;
use yii\base\Model;

class CsvForm extends Model
{
    public $file;

    public function rules(){
        return [
            [['file'],'required'],
            [['file'],'file','extensions'=>'csv','maxSize'=>1024 * 1024 * 10],
        ];
    }

    public function attributeLabels(){
        return [
            'file'=>'Select File',
        ];
    }
}