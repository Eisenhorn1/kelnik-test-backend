<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */

$this->title = 'Импорт файла';
?>
<div class="site-index">

    <div class="jumbotron">
        <p class="lead">Импорт csv файла</p>

        <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
        <?= $form->field($csvModel,'file')->fileInput(['class' => 'b-btn__upload'])->label(false) ?>
        <div class="form-group">
            <?= Html::submitButton('Отправить',['class'=>'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

</div>
