<?php
/**
 * Created by PhpStorm.
 * User: lichrok
 * Date: 12.07.2018
 * Time: 11:37
 */
use yii\helpers\Html;
use yii\widgets\LinkPager;
?>
    <h1>Data</h1>
    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Articul</th>
                <th>Price</th>
                <th>Count</th>
            </tr>
        </thead>
        <?php foreach ($items as $item): ?>
            <tr>
                <td><?= $item->ID ?></td>
                <td><?= $item->ARTICUL ?></td>
                <td><?= $item->PRICE ?></td>
                <td><?= $item->COUNT ?></td>
            </tr>
        <?php endforeach; ?>
    </table>

<?= LinkPager::widget(['pagination' => $pagination]) ?>