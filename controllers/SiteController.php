<?php

namespace app\controllers;

use app\models\Catalog;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\CsvForm;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $csvModel = new CsvForm;

        if($csvModel->load(Yii::$app->request->post())){
            $file = UploadedFile::getInstance($csvModel,'file');
            $filename = 'Data.'.$file->extension;
            $upload = $file->saveAs('upload/'.$filename);
            if($upload){
                define('CSV_PATH','upload/');
                $csv_file = CSV_PATH . $filename;
                $filecsv = file($csv_file);

                foreach($filecsv as $data){
                    $modelnew = new Catalog;
                    $subString = explode(";",$data);
                    $articul = $subString[0];
                    $price = $subString[1];
                    $count = $subString[2];

                    $modelnew->ARTICUL = $articul;
                    $modelnew->PRICE = $price;
                    $modelnew->COUNT = $count;
                    $modelnew->save();
                }
                unlink('upload/'.$filename);
                return $this->redirect(['site/index']);
            }
        }

        return $this->render('index', compact('csvModel'));
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionUpload(){
        $model = new CsvForm;

        if($model->load(Yii::$app->request->post())){
            $file = UploadedFile::getInstance($model,'file');
            $filename = 'Data.'.$file->extension;
            $upload = $file->saveAs('upload/'.$filename);
            if($upload){
                define('CSV_PATH','upload/');
                $csv_file = CSV_PATH . $filename;
                $filecsv = file($csv_file);
                print_r($filecsv);
                foreach($filecsv as $data){
                    $modelnew = new Catalog;
                    $subString = explode(",",$data);
                    $id = $subString[0];
                    $articul = $subString[1];
                    $price = $subString[2];
                    $count = $subString[3];

                    $modelnew->id = $id;
                    $modelnew->articul = $articul;
                    $modelnew->price = $price;
                    $modelnew->count = $count;
                    $modelnew->save();
                }
                unlink('upload/'.$filename);
                return $this->redirect(['site/index']);
            }
        }else{
            return $this->render('upload',['model'=>$model]);
        }
    }
}
