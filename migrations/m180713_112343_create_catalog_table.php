<?php

use yii\db\Migration;

/**
 * Handles the creation of table `catalog`.
 */
class m180713_112343_create_catalog_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('catalog', [
            'ID' => $this->primaryKey(),
            'ARTICUL' => $this->text(),
            'PRICE' => $this->integer(),
            'COUNT' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('catalog');
    }
}
